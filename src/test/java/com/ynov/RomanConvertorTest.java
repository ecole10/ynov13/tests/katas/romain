package com.ynov;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@QuarkusTest
public class RomanConvertorTest {
    private RomanConvertor romanConvertor;

    @BeforeEach
    public void setup() {
        romanConvertor = new RomanConvertor();
    }

    @AfterEach
    public void tear() {
        romanConvertor = null;
    }

    @Test
    public void roman_convertor_should_print_number_as_roman() {
        romanConvertor.print(3999);
    }

    @Test
    public void roman_convertor_should_return_I_when_number_is_1() {
        // When
        var result = romanConvertor.convert(1);

        // Then
        assertThat(result).isEqualTo("I");
    }

    @Test
    public void roman_convertor_should_return_V_when_number_is_5() {
        // When
        var result = romanConvertor.convert(5);

        // Then
        assertThat(result).isEqualTo("V");
    }

    @Test
    public void roman_convertor_should_return_X_when_number_is_10() {
        // When
        var result = romanConvertor.convert(10);

        // Then
        assertThat(result).isEqualTo("X");
    }

    @Test
    public void roman_convertor_should_return_L_when_number_is_50() {
        // When
        var result = romanConvertor.convert(50);

        // Then
        assertThat(result).isEqualTo("L");
    }

    @Test
    public void roman_convertor_should_return_C_when_number_is_100() {
        // When
        var result = romanConvertor.convert(100);

        // Then
        assertThat(result).isEqualTo("C");
    }

    @Test
    public void roman_convertor_should_return_D_when_number_is_500() {
        // When
        var result = romanConvertor.convert(500);

        // Then
        assertThat(result).isEqualTo("D");
    }

    @Test
    public void roman_convertor_should_return_M_when_number_is_1000() {
        // When
        var result = romanConvertor.convert(1000);

        // Then
        assertThat(result).isEqualTo("M");
    }

    @Test
    public void roman_convertor_should_return_II_when_number_is_2() {
        // When
        var result = romanConvertor.convert(2);

        // Then
        assertThat(result).isEqualTo("II");
    }

    @Test
    public void roman_convertor_should_return_IV_when_number_is_4() {
        // When
        var result = romanConvertor.convert(4);

        // Then
        assertThat(result).isEqualTo("IV");
    }

    @Test
    public void roman_convertor_should_return_IX_when_number_is_9() {
        // When
        var result = romanConvertor.convert(9);

        // Then
        assertThat(result).isEqualTo("IX");
    }
}
