package com.ynov;

import java.util.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;

public class RomanConvertor {
    private final List<Entry<String, Integer>> nums = List.of(
            new SimpleEntry<>("M", 1000),
            new SimpleEntry<>("CM", 900),
            new SimpleEntry<>("D", 500),
            new SimpleEntry<>("C", 100),
            new SimpleEntry<>("XC", 90),
            new SimpleEntry<>("L", 50),
            new SimpleEntry<>("X", 10),
            new SimpleEntry<>("IX", 9),
            new SimpleEntry<>("V", 5),
            new SimpleEntry<>("I", 1)
    );

    public void print(int max) {
        for (int i = 1; i <= max; i++) {
            System.out.println(convert(i));
        }
    }

    public String convert(int number) {
        String roman = "";

        for (int i = 0; i <= nums.size(); i++) {
            var item = nums.get(i);

            if (number >= item.getValue()) {
                int quotient = number / item.getValue();

                if (quotient > 3) {
                    roman = roman.concat(item.getKey());

                    if (i != 0) {
                        roman = roman.concat(nums.get(i - 1).getKey());
                    }
                } else {
                    roman = roman.concat(item.getKey().repeat(quotient));
                }

                number = number - quotient * item.getValue();

                if (number == 0) {
                    break;
                }
            }
        }

        return roman;
    }
}
